<?php
class Tomato_Utility_UserPaginator extends Zend_Paginator_Adapter_Iterator
{
	public function __construct(Iterator $iterator, $count)
	{
		parent::__construct($iterator);
		$this->_count = $count;
	}
}
