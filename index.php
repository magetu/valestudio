<?php
error_reporting(E_ALL);

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);

define('TOMATO_ROOT_DIR', dirname(__FILE__));
define('TOMATO_APP_DIR',  TOMATO_ROOT_DIR . DS . 'application');
define('TOMATO_LIB_DIR',  'vietnam/lib');
define('TOMATO_TEMP_DIR', TOMATO_ROOT_DIR . DS . 'temp');
set_include_path(PS . TOMATO_LIB_DIR . PS . get_include_path());

/**
 * Run the application
 * Use Zend_Application
 * @since 2.0.3
 */
require_once 'Zend/Application.php';
$application = new Zend_Application(
    'production',
    TOMATO_APP_DIR . DS . 'config'. DS . 'application.ini'
);

/**
 * Don't store following options to application.ini, because when user try to install,
 * the installer can not save these options to application.ini
 * (it replaces TOMATO_APP_DIR with real path)
 */
$options = array(
	'bootstrap' => array(
    	'path' 	=> TOMATO_APP_DIR . DS . 'Bootstrap.php',
		'class' => 'Bootstrap',
    ),
    'autoloadernamespaces' => array(
    	'tomato' => 'Tomato_',
    ),
	'resources' => array(
		'frontController' => array(
			'controllerDirectory' => TOMATO_APP_DIR . DS . 'controllers',
			'moduleDirectory' 	  => TOMATO_APP_DIR . DS . 'modules',
		),
	),
);
$options = $application->mergeOptions($application->getOptions(), $options);
try {
	$application->setOptions($options)
	->bootstrap()
	->run();
} catch(Exception $ex) {
	//Header( "HTTP/1.1 301 Moved Permanently" );
	//Header( "Location: ".'/' );
	header("HTTP/1.0 404 Not Found",true,'404');
	$content = file_get_contents('./404.html');
	echo $content;
}

