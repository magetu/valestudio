<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload()
	{
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$modules = Tomato_Module_Loader::getInstance()->getModuleNames();
		new Tomato_Autoloader(array(
    			'basePath'  => TOMATO_APP_DIR,
    			'namespace' => 'Hooks_',
			));
		new Tomato_Autoloader(array(
    			'basePath'  => TOMATO_APP_DIR,
    			'namespace' => 'Plugins_',
			));
		foreach ($modules as $module) {
			new Tomato_Autoloader(array(
    			'basePath'  => TOMATO_APP_DIR . DS . 'modules' . DS . $module,
    			'namespace' => ucfirst($module) . '_',
			));
		}
		require_once 'htmlpurifier/HTMLPurifier/Bootstrap.php';
		HTMLPurifier_Bootstrap::registerAutoload();
		return $autoloader;
	}
	protected function _initInstallChecker()
	{
		$config = Tomato_Config::getConfig();
		if (null == $config->install || null == $config->install->date) {
			header('Location: install.php');
			exit;
		}
	}

	protected function _initRoutes()
	{
		$this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
		$routes = Tomato_Module_Loader::getInstance()->getRoutes();
		$front->setRouter($routes);
		$front->getRouter()->removeDefaultRoutes();
	}

	protected function _initSession()
	{
		Zend_Session::setSaveHandler(Core_Services_SessionHandler::getInstance());
		if (isset($_GET['PHPSESSID'])) {
			session_id($_GET['PHPSESSID']);
		} else if (isset($_POST['PHPSESSID'])) {
			session_id($_POST['PHPSESSID']);
		}
	}

	/**
	 * Add action helpers
	 * 
	 * @since 2.0.7
	 * @return void
	 */
	protected function _initActionHelpers()
	{
		/**
		 * Protect forms/pages from CSRF attacks
		 */
		Zend_Controller_Action_HelperBroker::addHelper(new Tomato_Controller_Action_Helper_Csrf());
		Zend_Controller_Action_HelperBroker::addPath(TOMATO_LIB_DIR . DS . 'Tomato' . DS . 'Controller' . DS . 'Action' . DS . 'Helper',
													 'Tomato_Controller_Action_Helper');
	}
	/**
	 * Register plugins
	 * 
	 * @return void
	 */
	protected function _initPlugins()
	{
		$this->bootstrap('FrontController');
		$front = $this->getResource('FrontController');
		/** 
		 * Register plugins
		 * The alternative way is that put plugin to /application/config/application.ini:
		 * resources.frontController.plugins.pluginName = "Plugin_Class"
		 */
		$front->registerPlugin(new Core_Controllers_Plugin_Init())
				->registerPlugin(new Tomato_Controller_Plugin_Admin())
		 		->registerPlugin(new Tomato_Controller_Plugin_Template())
		 		->registerPlugin(new Core_Controllers_Plugin_HookLoader())
		 		->registerPlugin(new Core_Controllers_Plugin_Auth())
		 		//->registerPlugin(new Plugins_Redirector_Plugin())
		 		;
	}
}