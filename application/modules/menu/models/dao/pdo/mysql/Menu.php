<?php
/**
 * TomatoCMS
 * 
 * LICENSE
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE Version 2 
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tomatocms.com so we can send you a copy immediately.
 * 
 * @copyright	Copyright (c) 2009-2010 TIG Corporation (http://www.tig.vn)
 * @license		http://www.gnu.org/licenses/gpl-2.0.txt GNU GENERAL PUBLIC LICENSE Version 2
 * @version 	$Id: Menu.php 5038 2010-08-28 17:43:29Z huuphuoc $
 * @since		2.0.5
 */

class Menu_Models_Dao_Pdo_Mysql_Menu
	extends Tomato_Model_Dao
	implements Menu_Models_Interface_Menu
{
	public function convert($entity) 
	{
		return new Menu_Models_Menu($entity); 
	}
	
	public function getById($id) 
	{
		$select = $this->_conn
						->select()
						->from(array('m' => $this->_prefix . 'menu'))
						->where('m.menu_id = ?', $id)
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Menu_Models_Menu($row);
	}
	
	public function add($menu) 
	{
		$this->_conn->insert($this->_prefix . 'menu', array(
				'name'		   => $menu->name,
				'description'  => $menu->description,
				'user_id'	   => $menu->user_id,
				'user_name'	   => $menu->user_name,
				'created_date' => $menu->created_date,
		
				/**
				 * @since 2.0.8
				 */
				'language'	   => $menu->language, 
			));
		return $this->_conn->lastInsertId($this->_prefix . 'menu');
	}
	
	public function update($menu) 
	{
		$where[] = 'menu_id = ' . $this->_conn->quote($menu->menu_id);
		return $this->_conn->update($this->_prefix . 'menu', array(
										'name'		  => $menu->name,
										'description' => $menu->description,
		
										/**
										 * @since 2.0.8
										 */
										'language'	  => $menu->language,
									), $where);
	}
	
	public function delete($id) 
	{
		$where[] = 'menu_id = ' . $this->_conn->quote($id);
		
		/**
		 * @since 2.0.7
		 */
		$this->_conn->delete($this->_prefix . 'menu_item', $where);
		
		return $this->_conn->delete($this->_prefix . 'menu', $where);
	}
	
	public function getMenus($offset = null, $count = null)
	{
		$select = $this->_conn
						->select()
						->from(array('m' => $this->_prefix . 'menu'))
						/**
						 * @since 2.0.8
						 */
						->where('m.language = ?', $this->_lang)
						->order('m.menu_id DESC');
		if (is_int($offset) && is_int($count)) {
			$select->limit($count, $offset);
		}
		$rs = $select->query()->fetchAll();
		return new Tomato_Model_RecordSet($rs, $this);
	}
	
	public function count()
	{
		$select = $this->_conn
						->select()
						->from(array('m' => $this->_prefix . 'menu'), array('num_menus' => 'COUNT(*)'))
						/**
						 * @since 2.0.8
						 */
						->where('m.language = ?', $this->_lang)
						->limit(1);
		$row = $select->query()->fetch();
		return $row->num_menus;
	}
	
	/* ========== For translation =========================================== */
	
	public function getTranslations($item)
	{
		$sql = 'SELECT m.* FROM ' . $this->_prefix . 'menu AS m
				INNER JOIN 
				(
					SELECT tr1.* FROM ' . $this->_prefix . 'core_translation AS tr1
					INNER JOIN ' . $this->_prefix . 'core_translation AS tr2 
						ON (tr1.item_id = ? AND tr1.source_item_id = tr2.item_id) 
						OR (tr2.item_id = ? AND tr1.item_id = tr2.source_item_id)
						OR (tr1.source_item_id = ? AND tr1.source_item_id = tr2.source_item_id)
					WHERE tr1.item_class = ? AND tr2.item_class = ?
					GROUP by tr1.translation_id
				) AS tr
					ON tr.item_id = m.menu_id';
		
		$rs = $this->_conn->query($sql, array(
										$item->menu_id, $item->menu_id, $item->menu_id, 
										'Menu_Models_Menu', 'Menu_Models_Menu'
								))->fetchAll();
		return (null == $rs) ? null : new Tomato_Model_RecordSet($rs, $this);
	}
	
	public function getTranslatable($lang)
	{
		$select = $this->_conn
						->select()
						->from(array('m' => $this->_prefix . 'menu'))
						->joinLeft(array('tr' => $this->_prefix . 'core_translation'), 
								'tr.source_item_id = m.menu_id 
								AND tr.item_class = "Menu_Models_Menu" 
								AND tr.language = ' . $this->_conn->quote($lang),
								array('translatable' => '(tr.item_id IS NULL)'))
						->where('m.language = ?', $this->_lang);
		$rs = $select->query()->fetchAll();
		return new Tomato_Model_RecordSet($rs, $this);
	}
	
	public function getSource($menu)
	{
		$select = $this->_conn
						->select()
						->from(array('m' => $this->_prefix . 'menu'))
						->joinInner(array('tr' => $this->_prefix . 'core_translation'), 'm.menu_id = tr.source_item_id', array())
						->where('tr.item_id = ?', $menu->menu_id)
						->where('tr.item_class = ?', 'Menu_Models_Menu')
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Menu_Models_Menu($row);
	}
}
