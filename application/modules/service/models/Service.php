<?php
/**
 * @copyright Copyright (c) 2007-2009 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 12/8/2016 10:48:42 AM
 */

/// <summary>
/// Service_Models_Service object for domain mapped table 'service'.
/// </summary>
class Service_Models_Service extends Tomato_Model_Entity {
	protected $_properties = array(
		'service_id' => null,
		'name' => null,
		'slug' => null,
		'description' => null,
		'content' => null,
		'status' => null,
		'images_data' => null,
		'image_small' => null,
		'image_medium' => null,
		'main' => 0,
		'priority' => 0,
		'created_on' => 0,
		'updated_on' => 0
	);
}
?>
