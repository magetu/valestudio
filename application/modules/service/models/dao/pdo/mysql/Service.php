<?php
/**
 * @copyright Copyright (c) 2007-2009 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 12/8/2016 10:48:42 AM
 */

/// <summary>
/// ServiceDao for table 'service'.
/// </summary>

class Service_Models_Dao_Pdo_Mysql_Service extends Tomato_Model_Dao
	implements Service_Models_Interface_Service
{
	public function convert($service) {
		return new Service_Models_Service($service); 
	}
	
	public function getById($id) {
		$select = $this->_conn
						->select()
						->from(array('s' => $this->_prefix.'service'))
						->where('s.service_id = ?', $id)
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Service_Models_Service($row);	
	}
	
	public function getByUrl($url) {
		$select = $this->_conn
						->select()
						->from(array('s' => $this->_prefix.'service'))
						->where("s.url = '".$url."'")
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Service_Models_Service($row);	
	}
	public function add($service) {
		$this->_conn->insert($this->_prefix.'service', array(
			'name' => $service->name,
			'slug' => $service->slug,
			'meta_title' => $service->meta_title,
			'meta_description' => $service->meta_description,
			'description' => $service->description,
			'content' => $service->content,
			'status' => $service->status,
			'images_data' => $service->images_data,
			'image_small' => $service->image_small,
			'image_medium' => $service->image_medium,
			'main' => $service->main,
			'priority' => $service->priority,
			'created_on' => $service->created_on,
			'updated_on' => $service->updated_on
		));
		return $this->_conn->lastInsertId($this->_prefix.'service');
	}
	
	public function update($service) {
		$where[] = 'service_id = '.$this->_conn->quote($service->service_id);
		return $this->_conn->update($this->_prefix.'service', array(
				'name' => $service->name,
				'slug' => $service->slug,
				'meta_title' => $service->meta_title,
				'meta_description' => $service->meta_description,
				'description' => $service->description,
				'content' => $service->content,
				'status' => $service->status,
				'images_data' => $service->images_data,
				'image_small' => $service->image_small,
				'image_medium' => $service->image_medium,
				'main' => $service->main,
				'priority' => $service->priority,
				'created_on' => $service->created_on,
				'updated_on' => $service->updated_on
		), $where);			
	}
	public function updateStatus($id, $status) 
	{
		$where[] = 'service_id = ' . $this->_conn->quote($id);
		return $this->_conn->update($this->_prefix . 'service', array(
										'status' 			 => $status
									), $where);
	}
	public function find($start, $offset, $exp = null, $order = null) {
		$select = $this->_conn
				->select()
				->from(array('s' => $this->_prefix.'service'))
				->limit($offset, $start);
		if ($exp) {
			if (isset($exp['name'])) {
				$select->where('s.name LIKE \'%'.addslashes($exp['name']).'%\'');
			}
			if (isset($exp['keyword'])) {
				$select->where('s.name LIKE \'%'.addslashes($exp['keyword']).'%\'');
			}
		}
		if(null == $order) 
			$select->order('s.service_id DESC');
		else
			$select->order($order);
		$rs = $select->query()->fetchAll();
		return new Tomato_Model_RecordSet($rs, $this);
	}
	
	/**
	 * @param array $exp Search expression (@see find)
	 * @return int
	 */
	public function count($exp = null) {
		$select = $this->_conn
				->select()
				->from(array('s' => $this->_prefix.'service'), array('num_items' => 'COUNT(*)'));
		if ($exp) {
			if (isset($exp['name'])) {
				$select->where('s.name LIKE \'%'.addslashes($exp['name']).'%\'');
			}
			if (isset($exp['keyword'])) {
				$select->where('s.name LIKE \'%'.addslashes($exp['keyword']).'%\'');
			}
		}
		$row = $select->query()->fetch();
		return $row->num_items;
	}
	
	/**
	 * @param int $id
	 * @return int
	 */
	public function delete($id) {
		$where[] = 'service_id = '.$this->_conn->quote($id);
		return $this->_conn->delete($this->_prefix.'service', $where);
	}
}
?>
