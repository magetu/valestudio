<?php
/**
 * @copyright Copyright (c) 2007-2010 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 12/8/2016 10:48:42 AM
 */

/// <summary>
/// ServiceInterface for table 'Service'.
/// </summary>

interface Service_Models_Interface_Service{
	
	public function getById($id);
	public function getByUrl($url);
	/**
	 * Add new service
	 * 
	 * @param Service_Models_Service $service
	 * @return int
	 */
	public function add($service);
	
	/**
	 * Update service
	 * 
	 * @param Service_Models_Service $service
	 * @return int
	 */
	public function update($service);
	
	public function updateStatus($id, $status) ;
	/**
	 * @param int $start
	 * @param int $offset
	 * @param array $exp Search expression. An array contain various conditions, keys including:
	 * 'keyword', 'name'
	 * @return Tomato_Models_RecordSet
	 */
	public function find($start, $offset, $exp = null);
	
	/**
	 * @param array $exp Search expression (@see find)
	 * @return int
	 */
	public function count($exp = null);
	
	/**
	 * @param int $id
	 * @return int
	 */
	public function delete($id);
}
?>
