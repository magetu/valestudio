﻿<?php
/**
 * Vietnamtravelguide - http://www.vietnamtravelguide.com/
 * Copyright (c) 2008-2010 Vietnamtravelguide Company (http://www.vietnamtravelguide.com)
 */

class Service_ServiceController extends Zend_Controller_Action {
	/* ========== Frontend actions ========================================== */
	public function indexAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
		$serviceDao->setDbConnection($conn);
		$services = $serviceDao->find(0,10,array(),'priority asc');
		$this->view->assign('services', $services);
	}
	
	public function detailAction() {
		$url = $this->_request->getParam('url');
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
		$serviceDao->setDbConnection($conn);
		$service = $serviceDao->getByUrl($url);
		$this->view->assign('service', $service);
	}
	
	/* ========== Backend actions =========================================== */
	
	public function addAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		if ($this->_request->isPost()) {
			$user = Zend_Auth::getInstance()->getIdentity();
			$purifier = new HTMLPurifier();
			$name = strip_tags($this->_request->getPost('name'));
			$slug = strip_tags($this->_request->getPost('slug'));
			$meta_title = strip_tags($this->_request->getPost('meta_title'));
			$meta_description = strip_tags($this->_request->getPost('meta_description'));
			$description = strip_tags($this->_request->getPost('description'));
			$content = $purifier->purify($this->_request->getPost('content'));
			$status = $this->_request->getPost('status');
			$images_data = $purifier->purify($this->_request->getPost('images_data'));
			$main = $this->_request->getPost('main');
			$priority = $this->_request->getPost('priority');
			$service = new Service_Models_Service(array(
					'name' => $name,
					'slug' => $slug,
					'meta_title' => $meta_title,
					'meta_description' => $meta_description,
					'description' => $description,
					'content' => $content,
					'status' => $status,
					'images_data' => $images_data,
					'main' => $main,
					'priority' => $priority,
					'created_user_name' => $user->user_name,
			));
			$serviceImage = $this->_request->getPost('serviceImage');
			$imageUrls 	  = Zend_Json::decode(stripslashes($serviceImage));
			if (null != $imageUrls) {
				$service->image_small = $imageUrls['small'];
				$service->image_medium = $imageUrls['original'];
			}
			$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
			$serviceDao->setDbConnection($conn);
			$id = $serviceDao->add($service);
			if ($id > 0) {				
				$this->_helper->getHelper('FlashMessenger')->addMessage(
					$this->view->translator('service_add_success')
				);
				$this->_redirect('/admin/service/service/add/');
			}else{
				$this->_helper->getHelper('FlashMessenger')->addMessage(
					$this->view->translator('service_add_unsuccess')
				);	
			}
		}
	}
	
	public function editAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
		$serviceDao->setDbConnection($conn);
		$id = $this->_request->getParam('id');
		$service = $serviceDao->getById($id);
		if($service == null){
			$this->_redirect($this->view->serverUrl() . $this->view->url(array(), 'service_admin_service_index'));
		}
		$this->view->assign('service', $service);
		$this->view->assign('serviceImages', Zend_Json::encode(array(
			'small'     => $service->image_small,
			'medium'    => $service->image_medium,
		)));
		if ($this->_request->isPost()) {
			$purifier = new HTMLPurifier();
			$user = Zend_Auth::getInstance()->getIdentity();
			$service->name = strip_tags($this->_request->getPost('name'));
			$service->slug = strip_tags($this->_request->getPost('slug'));
			$service->meta_title = strip_tags($this->_request->getPost('meta_title'));
			$service->meta_description = strip_tags($this->_request->getPost('meta_description'));
			$service->description = strip_tags($this->_request->getPost('description'));
			$service->content = $purifier->purify($this->_request->getPost('content'));
			$service->status = $this->_request->getPost('status');

            // Remove image data and delete image file when do delete image
            if ($service->images_data != '[]' && $purifier->purify($this->_request->getPost('images_data')) == '[]') {
				$imagesDataOri = rtrim($service->images_data, ']');
				$imagesDataOri = ltrim($imagesDataOri, '[');
				$imagesDataOri = json_decode($imagesDataOri, true);
                $originalImage = $imagesDataOri['original']['url'];
                $squareImage = $imagesDataOri['square']['url'];
                $smallImage = $imagesDataOri['small']['url'];
                unlink(TOMATO_ROOT_DIR . $originalImage);
                unlink(TOMATO_ROOT_DIR . $squareImage);
                unlink(TOMATO_ROOT_DIR . $smallImage);
                $service->image_small = '';
                $service->image_medium = '';
			} else {
                $serviceImage = $this->_request->getPost('serviceImage');
                $imageUrls 	  = Zend_Json::decode(stripslashes($serviceImage));
                if (null != $imageUrls) {
                    $service->image_small = $imageUrls['small'];
                    $service->image_medium = $imageUrls['original'];
                }
            }

			$service->images_data = $purifier->purify($this->_request->getPost('images_data'));
			$service->main = $this->_request->getPost('main');
			$service->priority = $this->_request->getPost('priority');
			$service->updated_user_name = $user->user_name;

			$serviceDao->update($service);
			$this->_redirect('/admin/service/service/list/');
		}
	}
	
	public function listAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
		$serviceDao->setDbConnection($conn);
		$perPage = 20;
		$pageIndex = $this->getRequest()->getParam('pageIndex');
		if (null == $pageIndex || '' == $pageIndex) {
			$pageIndex = 1;
		}
		$start = ($pageIndex - 1) * $perPage;
		$this->view->assign('pageIndex', $pageIndex);
		
		// Build article search expression
		$paramsString = null;
		$exp = array();
		
		if ($this->getRequest()->isPost()) {
			$id = $this->getRequest()->getPost('id');
			$keyword = $this->getRequest()->getPost('keyword');
			if ($keyword) {
				$exp['keyword'] = $keyword;
			}
			if ($id) {
				$exp['id'] = $id;
			}
			$paramsString = rawurlencode(base64_encode(Zend_Json::encode($exp)));
		} else {
			$paramsString = $this->getRequest()->getParam('q');
			if (null != $paramsString) {
				$exp = rawurldecode(base64_decode($paramsString));
				$exp = Zend_Json::decode($exp); 
			} else {
				$paramsString = rawurlencode(base64_encode(Zend_Json::encode($exp)));
			}
		}
		
		
		$services = $serviceDao->find($start, $perPage, $exp);
		$numItems = $serviceDao->count($exp);
		$this->view->assign('numItems', $numItems);
		$this->view->assign('services', $services);
		$this->view->assign('exp', $exp);
		/**
		 * Paginator
		 */
		$paginator = new Zend_Paginator(new Tomato_Utility_PaginatorAdapter($services, $numItems));
		$paginator->setCurrentPageNumber($pageIndex);
		$paginator->setItemCountPerPage($perPage);
		$this->view->assign('paginator', $paginator);
		$this->view->assign('paginatorOptions', array(
			'path'	   => $this->view->url(array(), 'service_admin_service_index'),
			'itemLink' => (null == $paramsString)
							? 'page-%d/'
							: 'page-%d/?q=' . $paramsString,
		));
	}	
	public function deleteAction() {
		$this->_helper->getHelper('layout')->disableLayout();
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		
		$result = 'RESULT_ERROR';
		if ($this->_request->isPost()) {
			$id = $this->_request->getPost('id');
						
			$conn = Tomato_Db_Connection::factory()->getMasterConnection();
			$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
			$serviceDao->setDbConnection($conn);
			$serviceDao->delete($id);
			$result = 'RESULT_OK';
		}
		$this->getResponse()->setBody($result);
	}
	public function activeAction() 
	{
		$request = $this->getRequest();
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if ($request->isPost()) {
			$status = ($request->getPost('status') == 'active') ? 'inactive' : 'active';
			$id     = $request->getPost('id');
			$conn = Tomato_Db_Connection::factory()->getMasterConnection();
			$serviceDao = Tomato_Model_Dao_Factory::getInstance()->setModule('service')->getServiceDao();
			$serviceDao->setDbConnection($conn);
			$serviceDao->updateStatus($id, $status);
			$this->getResponse()->setBody($status);
		}
	}
}
