<?php
/**
 * TomatoCMS
 * 
 * LICENSE
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE Version 2 
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tomatocms.com so we can send you a copy immediately.
 * 
 * @copyright	Copyright (c) 2009-2010 TIG Corporation (http://www.tig.vn)
 * @license		http://www.gnu.org/licenses/gpl-2.0.txt GNU GENERAL PUBLIC LICENSE Version 2
 * @version 	$Id: Mail.php 3352 2010-06-28 06:16:48Z huuphuoc $
 * @since		2.0.6
 */

class Mail_Models_Mail extends Tomato_Model_Entity 
{
	/**
	 * Mail variables that user can use in the mail template
	 */
	
	/**
	 * @const string
	 */
	const MAIL_VARIABLE_USERNAME = '%user_name%';
	
	/**
	 * @const string
	 */
	const MAIL_VARIABLE_EMAIL 	 = '%user_email%';
	
	protected $_properties = array(
		'mail_id' 	  	  => null,
		'template_id' 	  => null,
		'subject' 		  => null,
		'content' 		  => null,
		'created_user_id' => null,
		'from_mail' 	  => null,
		'from_name' 	  => null,
		'reply_to_mail'   => null,
		'reply_to_name'   => null,
		'to_mail' 		  => null,
		'to_name'         => null,
		'status' 		  => null,
		'created_date'    => null,
		'sent_date' 	  => null,
	);
}
