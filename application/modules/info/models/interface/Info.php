<?php
/**
 * @copyright Copyright (c) 2007-2010 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 12/8/2016 2:49:38 PM
 */

/// <summary>
/// InfoInterface for table 'Info'.
/// </summary>

interface Info_Models_Interface_Info{
	
	public function getById($id);
	public function getByUrl($url);
	/**
	 * Add new info
	 * 
	 * @param Info_Models_Info $info
	 * @return int
	 */
	public function add($info);
	
	/**
	 * Update info
	 * 
	 * @param Info_Models_Info $info
	 * @return int
	 */
	public function update($info);
	
	public function updateStatus($id, $status) ;
	/**
	 * @param int $start
	 * @param int $offset
	 * @param array $exp Search expression. An array contain various conditions, keys including:
	 * 'keyword', 'name'
	 * @return Tomato_Models_RecordSet
	 */
	public function find($start, $offset, $exp = null);
	
	/**
	 * @param array $exp Search expression (@see find)
	 * @return int
	 */
	public function count($exp = null);
	
	/**
	 * @param int $id
	 * @return int
	 */
	public function delete($id);
}
?>
