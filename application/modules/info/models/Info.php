<?php
/**
 * @copyright Copyright (c) 2007-2009 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 12/8/2016 2:49:38 PM
 */

/// <summary>
/// Info_Models_Info object for domain mapped table 'info'.
/// </summary>
class Info_Models_Info extends Tomato_Model_Entity {
	protected $_properties = array(
		'info_id' => null,
		'name' => null,
		'slug' => null,
		'description' => null,
		'content' => null,
		'status' => null,
		'images_data' => null,
		'image_small' => null,
		'image_medium' => null,
		'created_on' => 0,
		'updated_on' => 0
	);
}
?>
