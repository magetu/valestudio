<?php
/**
 * @copyright Copyright (c) 2007-2009 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 12/8/2016 2:49:38 PM
 */

/// <summary>
/// InfoDao for table 'info'.
/// </summary>

class Info_Models_Dao_Pdo_Mysql_Info extends Tomato_Model_Dao
	implements Info_Models_Interface_Info
{
	public function convert($info) {
		return new Info_Models_Info($info); 
	}
	
	public function getById($id) {
		$select = $this->_conn
						->select()
						->from(array('i' => $this->_prefix.'info'))
						->where('i.info_id = ?', $id)
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Info_Models_Info($row);	
	}
	
	public function getByUrl($url) {
		$select = $this->_conn
						->select()
						->from(array('i' => $this->_prefix.'info'))
						->where("i.slug = '".$url."'")
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Info_Models_Info($row);	
	}
	public function add($info) {
		$this->_conn->insert($this->_prefix.'info', array(
			'name' => $info->name,
			'slug' => $info->slug,
			'description' => $info->description,
			'content' => $info->content,
			'status' => $info->status,
			'image_banner' => $info->image_banner,
			'images_data' => $info->images_data,
			'image_small' => $info->image_small,
			'image_medium' => $info->image_medium,
			'created_on' => $info->created_on,
			'updated_on' => $info->updated_on
		));
		return $this->_conn->lastInsertId($this->_prefix.'info');
	}
	
	public function update($info) {
		$where[] = 'info_id = '.$this->_conn->quote($info->info_id);
		return $this->_conn->update($this->_prefix.'info', array(
				'name' => $info->name,
				'slug' => $info->slug,
				'description' => $info->description,
				'content' => $info->content,
				'image_banner' => $info->image_banner,
				'status' => $info->status,
				'images_data' => $info->images_data,
				'image_small' => $info->image_small,
				'image_medium' => $info->image_medium,
				'created_on' => $info->created_on,
				'updated_on' => $info->updated_on
		), $where);			
	}
	public function updateStatus($id, $status) 
	{
		$where[] = 'info_id = ' . $this->_conn->quote($id);
		return $this->_conn->update($this->_prefix . 'info', array(
										'status' 			 => $status
									), $where);
	}
	public function find($start, $offset, $exp = null, $order = null) {
		$select = $this->_conn
				->select()
				->from(array('i' => $this->_prefix.'info'))
				->limit($offset, $start);
		// Don't get item 'Contact'
		$select->where("i.slug != 'contact'");

		if ($exp) {
			if (isset($exp['name'])) {
				$select->where('i.name LIKE \'%'.addslashes($exp['name']).'%\'');
			}
			if (isset($exp['keyword'])) {
				$select->where('i.name LIKE \'%'.addslashes($exp['keyword']).'%\'');
			}
		}
		if(null == $order) 
			$select->order('i.info_id DESC');
		else
			$select->order($order);
		$rs = $select->query()->fetchAll();
		return new Tomato_Model_RecordSet($rs, $this);
	}
	
	/**
	 * @param array $exp Search expression (@see find)
	 * @return int
	 */
	public function count($exp = null) {
		$select = $this->_conn
				->select()
				->from(array('i' => $this->_prefix.'info'), array('num_items' => 'COUNT(*)'));
		if ($exp) {
			if (isset($exp['name'])) {
				$select->where('i.name LIKE \'%'.addslashes($exp['name']).'%\'');
			}
			if (isset($exp['keyword'])) {
				$select->where('i.name LIKE \'%'.addslashes($exp['keyword']).'%\'');
			}
		}
		$row = $select->query()->fetch();
		return $row->num_items;
	}
	
	/**
	 * @param int $id
	 * @return int
	 */
	public function delete($id) {
		$where[] = 'info_id = '.$this->_conn->quote($id);
		return $this->_conn->delete($this->_prefix.'info', $where);
	}
}
?>
