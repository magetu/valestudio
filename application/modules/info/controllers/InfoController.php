﻿<?php
/**
 * Vietnamtravelguide - http://www.vietnamtravelguide.com/
 * Copyright (c) 2008-2010 Vietnamtravelguide Company (http://www.vietnamtravelguide.com)
 */

class Info_InfoController extends Zend_Controller_Action {
	/* ========== Frontend actions ========================================== */
	public function indexAction() {
		$url = 'info';
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
		$infoDao->setDbConnection($conn);
		$info = $infoDao->getByUrl($url);
		$this->view->assign('info', $info);
	}

	public function joinourfamilyAction() {
		$url = 'join-our-family';
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
		$infoDao->setDbConnection($conn);
		$info = $infoDao->getByUrl($url);
		$this->view->assign('info', $info);
	}

	public function meetourteamAction() {
		$url = 'meet-our-team';
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
		$infoDao->setDbConnection($conn);
		$info = $infoDao->getByUrl($url);
		$this->view->assign('info', $info);
	}
	
	public function contactAction() {
		$url = 'contact';
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
		$infoDao->setDbConnection($conn);
		$info = $infoDao->getByUrl($url);
		$this->view->assign('info', $info);
	}
	public function sendcontactAction(){
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		$request	= $this->getRequest();
		$result = 'RESULT_ERROR';

		if ($request->isPost()) {
			$purifier = new HTMLPurifier();
			$name 	= trim($purifier->purify($request->getPost('name')));
			$email 	= $purifier->purify($request->getPost('email'));
			$company_name     = $request->getPost('company_name');
			$phone     = $request->getPost('phone');
			$message     = $request->getPost('message');

			$content = "";
			$content .="Name: " . $name . '<br>';
			$content .="Phone: " . $phone . '<br>';
			$content .="Company: " . $company_name . '<br>';
			$content .="Email: " . $email . '<br>';
			$content .="Message: " . $message . '<br>';

			if($name && $email && $phone && $company_name && $message) {
				$fromName    = $name;
				$fromMail    = $email;
				$replyToName = $name;
				$replyToMail = $email;
				
				// Choose one of way to send mail: SMTP or Mailer

				// Send mail with SMTP
				$mailhost= 'smtp.gmail.com';
				$mailconfig = array(
				    'auth'     => 'login',
				    'username' => 'tuvd.mail@gmail.com',
				    'password' => 'random@#',
				    'port'     => '465',
				    'ssl'      => 'ssl'
				);

				$transport = new Zend_Mail_Transport_Smtp($mailhost, $mailconfig);
				Zend_Mail::setDefaultTransport($transport);
				
				// Send Mail with Mailer
				//$transport = Mail_Services_Mailer::getMailTransport();

				$mail = new Zend_Mail('UTF-8');
				$mail->clearFrom()->setFrom($fromMail, $fromName)
				->clearReplyTo()->setReplyTo($replyToMail, $replyToName)
				->addTo('mrt@junowebdesign.com', "Mrt@junowebdesign.com")//vale@vale-studio.org
				->setSubject('ValeStudio.Org - Contact from ' . $name)
				->setBodyHtml($content)
				->send($transport);

				$result = 'RESULT_OK';
			} else {
				$result = 'RESULT_ERROR';
			}
		} else {
			$result = 'RESULT_ERROR';
		}
		
		$this->getResponse()->setBody($result);
	}
	
	/* ========== Backend actions =========================================== */
	
	public function addAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		if ($this->_request->isPost()) {
			$user = Zend_Auth::getInstance()->getIdentity();
			$purifier = new HTMLPurifier();
			$name = strip_tags($this->_request->getPost('name'));
			$slug = strip_tags($this->_request->getPost('slug'));
			$description = $this->_request->getPost('description');
			$content = $purifier->purify($this->_request->getPost('content'));
			$status = $this->_request->getPost('status');
			$images_data = $purifier->purify($this->_request->getPost('images_data'));
			$info = new Info_Models_Info(array(
					'name' => $name,
					'slug' => $slug,
					'description' => $description,
					'content' => $content,
					'status' => $status,
					'images_data' => $images_data,
					'created_user_name' => $user->user_name,
			));
			$infoImage = $this->_request->getPost('infoImage');
			$imageUrls 	  = Zend_Json::decode(stripslashes($infoImage));
			if (null != $imageUrls) {
				$info->image_square = $imageUrls['square'];
				$info->image_small = $imageUrls['small'];
				$info->image_medium = $imageUrls['original'];
			}
			$info->image_banner = $this->_request->getPost('image_banner');
			$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
			$infoDao->setDbConnection($conn);
			$id = $infoDao->add($info);
			if ($id > 0) {				
				$this->_helper->getHelper('FlashMessenger')->addMessage(
					$this->view->translator('info_add_success')
				);
				$this->_redirect('/admin/info/info/add/');
			}else{
				$this->_helper->getHelper('FlashMessenger')->addMessage(
					$this->view->translator('info_add_unsuccess')
				);	
			}
		}
	}
	
	public function editAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
		$infoDao->setDbConnection($conn);
		$id = $this->_request->getParam('id');
		$info = $infoDao->getById($id);
		if($info == null){
			$this->_redirect($this->view->serverUrl() . $this->view->url(array(), 'info_admin_info_index'));
		}
		$this->view->assign('info', $info);
		$this->view->assign('infoImages', Zend_Json::encode(array(
			'square'    => $info->image_square,
			'small'     => $info->image_small,
			'medium'    => $info->image_medium,
		)));
		if ($this->_request->isPost()) {
			$purifier = new HTMLPurifier();
			$user = Zend_Auth::getInstance()->getIdentity();
			$info->name = strip_tags($this->_request->getPost('name'));
			$info->slug = strip_tags($this->_request->getPost('slug'));
			$info->description = $this->_request->getPost('description');
			$info->content = $purifier->purify($this->_request->getPost('content'));
			$info->status = $this->_request->getPost('status');

            // Remove image data and delete image file when do delete image
            if ($info->images_data != '[]' && $purifier->purify($this->_request->getPost('images_data')) == '[]') {
				$imagesDataOri = rtrim($info->images_data, ']');
				$imagesDataOri = ltrim($imagesDataOri, '[');
				$imagesDataOri = json_decode($imagesDataOri, true);
                $originalImage = $imagesDataOri['original']['url'];
                $squareImage = $imagesDataOri['square']['url'];
                $smallImage = $imagesDataOri['small']['url'];
                unlink(TOMATO_ROOT_DIR . $originalImage);
                unlink(TOMATO_ROOT_DIR . $squareImage);
                unlink(TOMATO_ROOT_DIR . $smallImage);
                $info->image_small = '';
                $info->image_medium = '';
			} else {
				$infoImage = $this->_request->getPost('infoImage');
				$imageUrls = Zend_Json::decode(stripslashes($infoImage));
				if (null != $imageUrls) {
					$info->image_square = $imageUrls['square'];
					$info->image_small = $imageUrls['small'];
					$info->image_medium = $imageUrls['original'];
				}
            }

			$info->images_data = $purifier->purify($this->_request->getPost('images_data'));
			$info->image_banner = $purifier->purify($this->_request->getPost('image_banner'));
			$info->updated_user_name = $user->user_name;
			
			$infoDao->update($info);
			$this->_redirect('/admin/info/info/list/');
		}
	}
	
	public function listAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
		$infoDao->setDbConnection($conn);
		$perPage = 20;
		$pageIndex = $this->getRequest()->getParam('pageIndex');
		if (null == $pageIndex || '' == $pageIndex) {
			$pageIndex = 1;
		}
		$start = ($pageIndex - 1) * $perPage;
		$this->view->assign('pageIndex', $pageIndex);
		
		// Build article search expression
		$paramsString = null;
		$exp = array();
		
		if ($this->getRequest()->isPost()) {
			$id = $this->getRequest()->getPost('id');
			$keyword = $this->getRequest()->getPost('keyword');
			if ($keyword) {
				$exp['keyword'] = $keyword;
			}
			if ($id) {
				$exp['id'] = $id;
			}
			$paramsString = rawurlencode(base64_encode(Zend_Json::encode($exp)));
		} else {
			$paramsString = $this->getRequest()->getParam('q');
			if (null != $paramsString) {
				$exp = rawurldecode(base64_decode($paramsString));
				$exp = Zend_Json::decode($exp); 
			} else {
				$paramsString = rawurlencode(base64_encode(Zend_Json::encode($exp)));
			}
		}
		
		$infos = $infoDao->find($start, $perPage, $exp);
		$numItems = $infoDao->count($exp);
		$this->view->assign('numItems', $numItems);
		$this->view->assign('infos', $infos);
		$this->view->assign('exp', $exp);
		/**
		 * Paginator
		 */
		$paginator = new Zend_Paginator(new Tomato_Utility_PaginatorAdapter($infos, $numItems));
		$paginator->setCurrentPageNumber($pageIndex);
		$paginator->setItemCountPerPage($perPage);
		$this->view->assign('paginator', $paginator);
		$this->view->assign('paginatorOptions', array(
			'path'	   => $this->view->url(array(), 'info_admin_info_index'),
			'itemLink' => (null == $paramsString)
							? 'page-%d/'
							: 'page-%d/?q=' . $paramsString,
		));
	}	
	public function deleteAction() {
		$this->_helper->getHelper('layout')->disableLayout();
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		
		$result = 'RESULT_ERROR';
		if ($this->_request->isPost()) {
			$id = $this->_request->getPost('id');
						
			$conn = Tomato_Db_Connection::factory()->getMasterConnection();
			$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
			$infoDao->setDbConnection($conn);
			$infoDao->delete($id);
			$result = 'RESULT_OK';
		}
		$this->getResponse()->setBody($result);
	}
	public function activeAction() 
	{
		$request = $this->getRequest();
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if ($request->isPost()) {
			$status = ($request->getPost('status') == 'active') ? 'inactive' : 'active';
			$id     = $request->getPost('id');
			$conn = Tomato_Db_Connection::factory()->getMasterConnection();
			$infoDao = Tomato_Model_Dao_Factory::getInstance()->setModule('info')->getInfoDao();
			$infoDao->setDbConnection($conn);
			$infoDao->updateStatus($id, $status);
			$this->getResponse()->setBody($status);
		}
	}
}