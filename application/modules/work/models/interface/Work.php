<?php
/**
 * @copyright Copyright (c) 2007-2010 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 13/8/2016 9:35:28 AM
 */

/// <summary>
/// WorkInterface for table 'Work'.
/// </summary>

interface Work_Models_Interface_Work{
	
	public function getById($id);
	public function getByUrl($url);
	/**
	 * Add new work
	 * 
	 * @param Work_Models_Work $work
	 * @return int
	 */
	public function add($work);
	
	/**
	 * Update work
	 * 
	 * @param Work_Models_Work $work
	 * @return int
	 */
	public function update($work);
	
	public function updateStatus($id, $status) ;
	/**
	 * @param int $start
	 * @param int $offset
	 * @param array $exp Search expression. An array contain various conditions, keys including:
	 * 'keyword', 'name'
	 * @return Tomato_Models_RecordSet
	 */
	public function find($start, $offset, $exp = null);
	
	/**
	 * @param array $exp Search expression (@see find)
	 * @return int
	 */
	public function count($exp = null);
	
	/**
	 * @param int $id
	 * @return int
	 */
	public function delete($id);
}
?>
