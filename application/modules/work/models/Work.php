<?php
/**
 * @copyright Copyright (c) 2007-2009 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 13/8/2016 9:35:28 AM
 */

/// <summary>
/// Work_Models_Work object for domain mapped table 'work'.
/// </summary>
class Work_Models_Work extends Tomato_Model_Entity {
	protected $_properties = array(
		'work_id' => null,
		'name' => null,
		'slug' => null,
		'description' => null,
		'type' => null,
		'vimeoId' => null,
		'priority' => 0,
		'status' => null,
		'images_data' => null,
		'image_medium' => null,
		'image_small' => null,
		'updated_on' => 0,
		'created_on' => 0
	);
}
?>
