<?php
/**
 * @copyright Copyright (c) 2007-2009 Architecture (tienbd@gmail.com)
 * @author Bui Duc Tien <tienbd@gmail.com>
 * @created 13/8/2016 9:35:28 AM
 */

/// <summary>
/// WorkDao for table 'work'.
/// </summary>

class Work_Models_Dao_Pdo_Mysql_Work extends Tomato_Model_Dao
	implements Work_Models_Interface_Work
{
	public function convert($work) {
		return new Work_Models_Work($work); 
	}
	
	public function getById($id) {
		$select = $this->_conn
						->select()
						->from(array('w' => $this->_prefix.'work'))
						->where('w.work_id = ?', $id)
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Work_Models_Work($row);	
	}
	
	public function getByUrl($url) {
		$select = $this->_conn
						->select()
						->from(array('w' => $this->_prefix.'work'))
						->where("w.url = '".$url."'")
						->limit(1);
		$row = $select->query()->fetch();
		return (null == $row) ? null : new Work_Models_Work($row);	
	}
	public function add($work) {
		$this->_conn->insert($this->_prefix.'work', array(
			'name' => $work->name,
			'slug' => $work->slug,
			'description' => $work->description,
			'type' => $work->type,
			'vimeoId' => $work->vimeoId,
			'priority' => $work->priority,
			'status' => $work->status,
			'is_home' => $work->is_home,
			'image_banner' => $work->image_banner,
			'thumb_video' => $work->thumb_video,
			'images_data' => $work->images_data,
			'image_medium' => $work->image_medium,
			'image_small' => $work->image_small,
			'updated_on' => $work->updated_on,
			'created_on' => $work->created_on
		));
		return $this->_conn->lastInsertId($this->_prefix.'work');
	}
	
	public function update($work) {
		$where[] = 'work_id = '.$this->_conn->quote($work->work_id);
		return $this->_conn->update($this->_prefix.'work', array(
				'name' => $work->name,
				'slug' => $work->slug,
				'description' => $work->description,
				'type' => $work->type,
				'vimeoId' => $work->vimeoId,
				'priority' => $work->priority,
				'status' => $work->status,
				'is_home' => $work->is_home,
				'image_banner' => $work->image_banner,
				'thumb_video' => $work->thumb_video,
				'images_data' => $work->images_data,
				'image_medium' => $work->image_medium,
				'image_small' => $work->image_small,
				'updated_on' => $work->updated_on,
				'created_on' => $work->created_on
		), $where);			
	}
	public function updateStatus($id, $status) 
	{
		$where[] = 'work_id = ' . $this->_conn->quote($id);
		return $this->_conn->update($this->_prefix . 'work', array(
										'status' 			 => $status
									), $where);
	}
	public function find($start, $offset, $exp = null, $order = null) {
		$select = $this->_conn
				->select()
				->from(array('w' => $this->_prefix.'work'))
				->limit($offset, $start);
		if ($exp) {
			if (isset($exp['name'])) {
				$select->where('w.name LIKE \'%'.addslashes($exp['name']).'%\'');
			}
			if (isset($exp['keyword'])) {
				$select->where('w.name LIKE \'%'.addslashes($exp['keyword']).'%\'');
			}
			if (isset($exp['status'])) {
				$select->where('w.status = \''.addslashes($exp['status']).'\'');
			} else {
				$select->where("w.status = 'active'");
			}
			if (isset($exp['is_home'])) {
				$select->where('w.is_home = ?', $exp['is_home']);
			}
		}
		if(null == $order) 
			$select->order('w.priority ASC');
		else
			$select->order($order);
		$rs = $select->query()->fetchAll();
		return new Tomato_Model_RecordSet($rs, $this);
	}
	
	/**
	 * @param array $exp Search expression (@see find)
	 * @return int
	 */
	public function count($exp = null) {
		$select = $this->_conn
				->select()
				->from(array('w' => $this->_prefix.'work'), array('num_items' => 'COUNT(*)'));
		if ($exp) {
			if (isset($exp['name'])) {
				$select->where('w.name LIKE \'%'.addslashes($exp['name']).'%\'');
			}
			if (isset($exp['keyword'])) {
				$select->where('w.name LIKE \'%'.addslashes($exp['keyword']).'%\'');
			}
			if (isset($exp['status'])) {
				$select->where('w.status = \''.addslashes($exp['status']).'\'');
			} else {
				$select->where("w.status = 'active'");
			}
			if (isset($exp['is_home'])) {
				$select->where('w.is_home = ?', $exp['is_home']);
			}
		}
		$row = $select->query()->fetch();
		return $row->num_items;
	}
	
	/**
	 * @param int $id
	 * @return int
	 */
	public function delete($id) {
		$where[] = 'work_id = '.$this->_conn->quote($id);
		return $this->_conn->delete($this->_prefix.'work', $where);
	}
}
?>
