﻿<?php
/**
 * Vietnamtravelguide - http://www.vietnamtravelguide.com/
 * Copyright (c) 2008-2010 Vietnamtravelguide Company (http://www.vietnamtravelguide.com)
 */

class Work_WorkController extends Zend_Controller_Action {
	/* ========== Frontend actions ========================================== */
	public function indexAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
		$workDao->setDbConnection($conn);
		$works = $workDao->find(0,1000,array(),'priority asc');
		$this->view->assign('works', $works);
	}
	
	public function detailAction() {
		$url = $this->_request->getParam('url');
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
		$workDao->setDbConnection($conn);
		$work = $workDao->getByUrl($url);
		$this->view->assign('work', $work);
	}
	
	/* ========== Backend actions =========================================== */
	
	public function addAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		if ($this->_request->isPost()) {
			$user = Zend_Auth::getInstance()->getIdentity();
			$purifier = new HTMLPurifier();
			$name = strip_tags($this->_request->getPost('name'));
			$slug = strip_tags($this->_request->getPost('slug'));
			$description = strip_tags($this->_request->getPost('description'));
			$type = strip_tags($this->_request->getPost('type'));
			$vimeoId = strip_tags($this->_request->getPost('vimeoId'));
			$priority = $this->_request->getPost('priority');
			$status = $this->_request->getPost('status');
			$images_data = $purifier->purify($this->_request->getPost('images_data'));
			$work = new Work_Models_Work(array(
					'name' => $name,
					'slug' => $slug,
					'description' => $description,
					'type' => $type,
					'vimeoId' => $vimeoId,
					'priority' => $priority,
					'status' => $status,
					'images_data' => $images_data,
					'created_user_name' => $user->user_name,
			));
			$workImage = $this->_request->getPost('workImage');
			$imageUrls 	  = Zend_Json::decode(stripslashes($workImage));
			if (null != $imageUrls) {
				$work->image_square = $imageUrls['square'];
				$work->image_small = $imageUrls['small'];
				$work->image_medium = $imageUrls['original'];
			}
			$work->is_home = $this->_request->getPost('is_home');
			$work->image_banner = $this->_request->getPost('image_banner');
			$work->thumb_video = $this->_request->getPost('thumb_video');
			$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
			$workDao->setDbConnection($conn);
			$id = $workDao->add($work);
			if ($id > 0) {				
				$this->_helper->getHelper('FlashMessenger')->addMessage(
					$this->view->translator('work_add_success')
				);
				$this->_redirect('/admin/work/work/add/');
			}else{
				$this->_helper->getHelper('FlashMessenger')->addMessage(
					$this->view->translator('work_add_unsuccess')
				);	
			}
		}
	}
	
	public function editAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
		$workDao->setDbConnection($conn);
		$id = $this->_request->getParam('id');
		$work = $workDao->getById($id);
		if($work == null){
			$this->_redirect($this->view->serverUrl() . $this->view->url(array(), 'work_admin_work_index'));
		}
		$this->view->assign('work', $work);
		$this->view->assign('workImages', Zend_Json::encode(array(
			'square'    => $work->image_square,
			'small'     => $work->image_small,
			'medium'    => $work->image_medium,
		)));
		if ($this->_request->isPost()) {
			$purifier = new HTMLPurifier();
			$user = Zend_Auth::getInstance()->getIdentity();
			$work->name = strip_tags($this->_request->getPost('name'));
			$work->slug = strip_tags($this->_request->getPost('slug'));
			$work->description = strip_tags($this->_request->getPost('description'));
			$work->type = strip_tags($this->_request->getPost('type'));
			$work->vimeoId = strip_tags($this->_request->getPost('vimeoId'));
			$work->priority = $this->_request->getPost('priority');
			$work->status = $this->_request->getPost('status');

            // Remove image data and delete image file when do delete image
            if ($work->images_data != '[]' && $purifier->purify($this->_request->getPost('images_data')) == '[]') {
				$imagesDataOri = rtrim($work->images_data, ']');
				$imagesDataOri = ltrim($imagesDataOri, '[');
				$imagesDataOri = json_decode($imagesDataOri, true);
                $originalImage = $imagesDataOri['original']['url'];
                $squareImage = $imagesDataOri['square']['url'];
                $smallImage = $imagesDataOri['small']['url'];
                unlink(TOMATO_ROOT_DIR . $originalImage);
                unlink(TOMATO_ROOT_DIR . $squareImage);
                unlink(TOMATO_ROOT_DIR . $smallImage);
                $work->image_small = '';
                $work->image_medium = '';
			} else {
                $serviceImage = $this->_request->getPost('serviceImage');
                $imageUrls 	  = Zend_Json::decode(stripslashes($serviceImage));
                if (null != $imageUrls) {
                    $work->image_small = $imageUrls['small'];
                    $work->image_medium = $imageUrls['original'];
                }
            }

			$work->images_data = $purifier->purify($this->_request->getPost('images_data'));
			$work->updated_user_name = $user->user_name;
			$work->image_banner = $purifier->purify($this->_request->getPost('image_banner'));
			$work->thumb_video = $purifier->purify($this->_request->getPost('thumb_video'));
			$work->is_home = $this->_request->getPost('is_home');
			
			$workDao->update($work);
			$this->_redirect('/admin/work/work/list/');
		}
	}
	
	public function listAction() {
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
		$workDao->setDbConnection($conn);
		$perPage = 20;
		$pageIndex = $this->getRequest()->getParam('pageIndex');
		if (null == $pageIndex || '' == $pageIndex) {
			$pageIndex = 1;
		}
		$start = ($pageIndex - 1) * $perPage;
		$this->view->assign('pageIndex', $pageIndex);
		
		// Build article search expression
		$paramsString = null;
		$exp = array();
		
		if ($this->getRequest()->isPost()) {
			$id = $this->getRequest()->getPost('id');
			$keyword = $this->getRequest()->getPost('keyword');
			if ($keyword) {
				$exp['keyword'] = $keyword;
			}
			if ($id) {
				$exp['id'] = $id;
			}
			$paramsString = rawurlencode(base64_encode(Zend_Json::encode($exp)));
		} else {
			$paramsString = $this->getRequest()->getParam('q');
			if (null != $paramsString) {
				$exp = rawurldecode(base64_decode($paramsString));
				$exp = Zend_Json::decode($exp); 
			} else {
				$paramsString = rawurlencode(base64_encode(Zend_Json::encode($exp)));
			}
		}
		
		
		$works = $workDao->find($start, $perPage, $exp);
		$numItems = $workDao->count($exp);
		$this->view->assign('numItems', $numItems);
		$this->view->assign('works', $works);
		$this->view->assign('exp', $exp);
		/**
		 * Paginator
		 */
		$paginator = new Zend_Paginator(new Tomato_Utility_PaginatorAdapter($works, $numItems));
		$paginator->setCurrentPageNumber($pageIndex);
		$paginator->setItemCountPerPage($perPage);
		$this->view->assign('paginator', $paginator);
		$this->view->assign('paginatorOptions', array(
			'path'	   => $this->view->url(array(), 'work_admin_work_index'),
			'itemLink' => (null == $paramsString)
							? 'page-%d/'
							: 'page-%d/?q=' . $paramsString,
		));
	}	
	public function deleteAction() {
		$this->_helper->getHelper('layout')->disableLayout();
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		
		$result = 'RESULT_ERROR';
		if ($this->_request->isPost()) {
			$id = $this->_request->getPost('id');
						
			$conn = Tomato_Db_Connection::factory()->getMasterConnection();
			$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
			$workDao->setDbConnection($conn);
			$workDao->delete($id);
			$result = 'RESULT_OK';
		}
		$this->getResponse()->setBody($result);
	}
	public function activeAction() 
	{
		$request = $this->getRequest();
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if ($request->isPost()) {
			$status = ($request->getPost('status') == 'active') ? 'inactive' : 'active';
			$id     = $request->getPost('id');
			$conn = Tomato_Db_Connection::factory()->getMasterConnection();
			$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
			$workDao->setDbConnection($conn);
			$workDao->updateStatus($id, $status);
			$this->getResponse()->setBody($status);
		}
	}
}
