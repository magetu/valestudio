<?php
class Work_Widgets_Banner_Widget extends Tomato_Widget 
{
	protected function _prepareShow() 
	{
		$conn = Tomato_Db_Connection::factory()->getMasterConnection();
		$workDao = Tomato_Model_Dao_Factory::getInstance()->setModule('work')->getWorkDao();
		$workDao->setDbConnection($conn);
		$galleries = $workDao->find(0,100,array('status'=>'active','is_home'=>1),'priority asc');
		$this->_view->assign('galleries',$galleries);
	}
}
