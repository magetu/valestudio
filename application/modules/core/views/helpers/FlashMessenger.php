<?php
class Core_View_Helper_FlashMessenger extends Zend_View_Helper_Abstract 
{
	public function flashMessenger() 
	{
		$this->view->addScriptPath(Zend_Layout::getMvcInstance()->getLayoutPath());
		$this->view->addScriptPath(TOMATO_APP_DIR . DS . 'modules' . DS . 'core' . DS . 'views' . DS . 'scripts');
		
		$flashMsgHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
		$this->view->assign('messages', $flashMsgHelper->getMessages()); 
		
		return $this->view->render('_partial/_messages.phtml');
	}
}
