<?php
class Core_View_Helper_Date extends Zend_View_Helper_Abstract
{
	public function date($format = 'l, M d Y H:i')
	{
	    $timezone  = +7; //(GMT +7:00) 
        echo gmdate($format, time() + 3600*($timezone+date("0"))); 
	}	
}
