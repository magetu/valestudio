<?php
/**
 * TomatoCMS
 * 
 * LICENSE
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE Version 2 
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tomatocms.com so we can send you a copy immediately.
 * 
 * @copyright	Copyright (c) 2009-2010 TIG Corporation (http://www.tig.vn)
 * @license		http://www.gnu.org/licenses/gpl-2.0.txt GNU GENERAL PUBLIC LICENSE Version 2
 * @version 	$Id: Log.php 3963 2010-07-25 09:11:00Z huuphuoc $
 * @since		2.0.7
 */

class Core_Models_Log extends Tomato_Model_Entity
{
	protected $_properties = array(
		'log_id'       => null,
		'created_date' => null,
		'uri'		   => null,
		'module'       => null,
		'controller'   => null,
		'action' 	   => null,
		'class' 	   => null,
		'file'		   => null,
		'line'		   => null,
		'message' 	   => null,
		'trace'		   => null,
	);
}
