<?php
class Core_Services_Captcha 
{
	//generates an instance of Zend_Captcha
	//returns ID of captcha session
	function generateCaptcha() {
		$captcha = new Zend_Captcha_Image();
	    $captcha->setTimeout('300')
	                    ->setWordLen('6')
	                    ->setHeight('80')
	                    ->setFont(TOMATO_ROOT_DIR.'/upload/captcha/font/impact.ttf')
	                    ->setImgDir(TOMATO_ROOT_DIR.'/upload/captcha/images');
	        $captcha->generate();    //command to generate session + create image
	        return $captcha->getId();   //returns the ID given to session &amp; image
	}   
	//end function generateCaptcha

    //validates captcha response
	function validateCaptcha($captcha) {
		$captchaId = $captcha['id'];
		$captchaInput = $captcha['input'];
		$captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaId);
		$captchaIterator = $captchaSession->getIterator();
		$captchaWord = $captchaIterator['word'];
		if($captchaWord) {
		   if( $captchaInput == $captchaWord ){
				return true;
		   }
		} 
	   return false;
	}
}