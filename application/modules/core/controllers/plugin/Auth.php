<?php
/**
 * Base on the request URL and role/permisson of current user, forward the user
 * to the login page if the user have not logged in 
 */
class Core_Controllers_Plugin_Auth extends Zend_Controller_Plugin_Abstract 
{
	public function preDispatch(Zend_Controller_Request_Abstract $request) 
	{
		$uri = $request->getRequestUri();
		$uri = strtolower($uri);

		$uri = rtrim($uri, '/') . '/';
		if (strpos($uri, '/admin/') === false) {
			return;
		}
		
		/**
		 * Switch to admin template
		 * @since 2.0.4
		 */
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
		if (null === $viewRenderer->view) {
			$viewRenderer->initView();
		}
		$view = $viewRenderer->view;
		$view->assign('APP_TEMPLATE', 'admin');
		Zend_Layout::startMvc(array('layoutPath' => TOMATO_APP_DIR . DS . 'templates' . DS . 'admin' . DS . 'layouts'));
		Zend_Layout::getMvcInstance()->setLayout('admin');
		
		$isAllowed = false;
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$user 		= Zend_Auth::getInstance()->getIdentity();
			$module 	= $request->getModuleName();
			$controller = $request->getControllerName();
			$action 	= $request->getActionName();
			
			/**
			 * Add 'core:message' resource that allows show the friendly error message
			 */
			$acl = Core_Services_Acl::getInstance();
			if (!$acl->has('core:message')) {
				$acl->addResource('core:message');
			}
			
			/**
			 * Alway allows logged in user to access the dashboard
			 * We should NOT use:
			 * <code>
			 * $currentRouteName = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
			 * if ('core_dashboard_index' == $currentRouteName) {
			 * 		$isAllowed = true;
			 * }
			 * </code>
			 * because this approach still throws exception when there are no routes matching with current URL
			 * @since 2.0.8
			 */
			if ('core_dashboard_index' == strtolower($module . '_' . $controller . '_' . $action)) {
				$isAllowed = true;
			} else {
				$isAllowed = $acl->isUserOrRoleAllowed($user->role_id, $user->user_id, $module, $controller, $action);
			}
		}
		if (!$isAllowed) {
			$forwardAction = Zend_Auth::getInstance()->hasIdentity() ? 'deny' : 'login';
			
			/**
			 * DON'T use redirect! as folow:
			 * $this->getResponse()->setRedirect('/Login/');
			 */
			$request->setModuleName('core')
					->setControllerName('Auth')
					->setActionName($forwardAction)
					->setDispatched(true);
		}
	}
}
