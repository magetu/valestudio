<?php

class Core_IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_helper->getHelper('layout')->setLayout('home');
		$this->_helper->getHelper('viewRenderer')->setNoRender();
		$this->view->headMeta()->setName('description', "Ordinary");
		$this->view->headTitle("Vale Studio");
	}
}
