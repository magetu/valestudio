var wh = $(window).height();
function showdetail(id) {
	$.ajax({
		  type: "POST",
		  url: '/ajax/team/showdetail',
		  data: {id: id },
		  success: function(response){
            $('.modal-content').html(response);
            $('#teamModal img').css('max-height', wh - 100);
		  },
	});
}

jQuery(document).ready(function($) {
    "use strict";

    /*==============================
     Masthead
     ==============================*/
    $(window).scroll(function(){ 
        if($(this).scrollTop() >= 100){ if( !$("#masthead").hasClass('scrolled') ){  $("#masthead").addClass('scrolled');}
        }else if($(this).scrollTop()<= 99) {$("#masthead").removeClass('scrolled');}
    });

    /*==============================
     Service page
     ==============================*/
    if (window.location.hash.substr(1) == 'aboutUs') {
        $('#aboutUs-link').addClass('active');
    }

    if (window.location.hash.substr(1) == 'whatWeDo') {
        $('#whatWeDo-link').addClass('active');
    }

    if (window.location.pathname == '/services.html') {
        $('a[href="/services.html#aboutUs"]').attr('href', 'javaScript:void(0);').addClass('moveToAboutUs');
        $('a[href="/services.html#whatWeDo"]').attr('href', 'javaScript:void(0);').addClass('moveToWhatWeDo');

        $('a.moveToAboutUs').bind('click', function()
            {
                $('html, body').animate({
                    scrollTop: $("#aboutUs").offset().top - 200
                }, 1500);
                
                $('#whatWeDo-link').removeClass('active');
                $('#aboutUs-link').addClass('active');
            });
            
        $('a.moveToWhatWeDo').bind('click', function()
            {
                $('html, body').animate({
                    scrollTop: $("#whatWeDo").offset().top - 200
                }, 1000);
                
                $('#aboutUs-link').removeClass('active');
                $('#whatWeDo-link').addClass('active');
            });
    }

    /*==============================
     Mobile check
     ==============================*/
    function mobilecheck() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return false;
        }
        return true;
    }
	
	var ww = $(window).width();

    /*==============================
     Banner
     ==============================*/
    $('#banner, .banner-slider .flex-viewport li, .banner-slider .item-img').height($(window).height());
    $(window).on('resize', function() {
        $('#banner, .banner-slider .flex-viewport li, .banner-slider .item-img').height($(window).height());
    });


    /*==============================
     BANNER SLIDER
     ==============================*/
    $(window).on('load', function() {
        $('.banner-slider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: false,
            slideshowSpeed: 7000,
            animationSpeed: 600,
            after: function(){
            	$('.caption').text($('.banner-slider .slides li.flex-active-slide').attr('title'));
            },
        });
        
        /*==============================
         COVER IMG SLIDER
         ==============================*/
        $(".banner-slider .slides img").css({
            opacity: '0',
            visibility: 'hidden'
        }).before(function () {
            var srcImg = $(this).attr('src');
            return '<div class="item-img" style="min-height: ' + jQuery(window).height() + 'px;background-image: url(' + srcImg + ')">';
        });
        

        //$('.banner-slider .caption').appendTo('.item-img');

    });

    $(window).load(function () {
        /*==============================
         Preloader
         ==============================*/
        $('#preloader').fadeOut(500);
    });

    var ww = $(window).width();

    /*==============================
     Menu
     ==============================*/
	 
	$('.menu-toggle').click(function(){
        $(this).toggleClass('on');
        $('#masthead-nav').toggleClass('toggled-on');
    });
	
	if(ww < 992){
		$('.secondary-nav').appendTo('#masthead');
	}

    /*==============================
     Work
     ==============================*/

    if(ww > 1281) {
        $('figure.video').css('background-size','469px 234px');
    } else if(ww < 1280 && ww > 767) {
        $('figure.video').css('background-size','314.5px 154px');
    } else if(ww < 768 && ww > 320) {
        $('figure.video').css('background-size','369px 154px');
    }
    
    var myTheme = window.myTheme || {},
        $win = $( window );

    myTheme.Isotope = function () {
        var isotopeContainer = $('.isotopeContainer');
        if( !isotopeContainer.length || !jQuery().isotope ) return;
        $win.load(function(){
            isotopeContainer.isotope({
                itemSelector: '.isotopeSelector'
            });
            $('.isotopeFilters').on( 'click', 'a', function(e) {
                $('.isotopeFilters').find('.active').removeClass('active');
                $(this).addClass('active');
                var filterValue = $(this).attr('data-filter');
                isotopeContainer.isotope({ filter: filterValue });
                e.preventDefault();
            });
        });
    };

    myTheme.Fancybox = function () {

        $(".fancybox-pop").fancybox({
            openEffect	: 'elastic',
            closeBtn : false,
            afterLoad: function() {
                this.title = '<h4>'+this.title+'</h4><p>'+ this.element.attr('rel') + '</p>';
            },
            helpers : {
                title : {
                    type: 'inside'
                },
            }
        });

        $('.fancybox-video').fancybox({
            helpers: {
                media: {}
            }
        });
    };

    myTheme.Fancybox();
    myTheme.Isotope();

    $(window).on('resize', function(){
        $('.isotopeContainer').isotope('layout')
    });

    // Service page
    $(".aboutUs-link").click(function() {
        $('html, body').animate({
            scrollTop: $("#aboutUs").offset().top - 200
        }, 1000);
        
        $('#whatWeDo-link').removeClass('active');
        $(this).addClass('active');
    });
    
    $(".whatWeDo-link").click(function() {
        $('html, body').animate({
            scrollTop: $("#whatWeDo").offset().top - 100
        }, 1000);
        
        $('#aboutUs-link').removeClass('active');
        $(this).addClass('active');
    });
    
    // Contact
    $('#btn-submit').click(function() {
        $('#frmContact').validate({
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    digits: true,
                    minlength:10,
                    maxlength:15
                },
                email: {
                    required: true,
                    email:true
                },
                company_name: {
                    required: true
                },
                message: {
                    required: true,
                    minlength:30
                }
            }
        });

        if($("#frmContact").valid()) {
            var name = $('#name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var company_name = $('#company_name').val();
            var message = $('#message').val();
            
            $('#loading-image').show();
            $.ajax({
                type: "POST",
                url: 'contact/sendcontact',
                data: { name: name, 
                        phone: phone,
                        email: email,
                        company_name: company_name,
                        message: message
                    },
                success: function(response) {
                    $('p.message').show();
                    
                    if (response == 'RESULT_OK') {
                        //Reset form
                        $('#name').val('');
                        $('#phone').val('');
                        $('#email').val('');
                        $('#company_name').val('');
                        $('#message').val('');

                        $('p.message').css('border', '2px solid #398f14');
                        $('p.message').text('Your message was sent successfully. Thanks.');
                    } else {
                        $('p.message').css('border', '2px solid #c7254e');
                        $('p.message').text('There was an error when sending contact. Please contact to email or phone. Thanks');
                    }
                },
                complete: function(){
                $('#loading-image').hide();
                }
            });
        }

        return false; // because we want to submit only through `ajax`, so stopping original form submit.
    });
});