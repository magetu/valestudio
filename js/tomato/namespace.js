function popitup(url) {
	newwindow=window.open(url,'name','height=300,width=400');
	if (window.focus) {newwindow.focus()}
	return false;
} 
String.prototype.namespace = function(separator) {
	var ns = this.split(separator || '.'),
    	o = window,
    	i,
    	len;
	for (i = 0, len = ns.length; i < len; i++) {
		o = o[ns[i]] = o[ns[i]] || {};
	}
	return o;
};
