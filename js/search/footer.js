$(document).ready(function(){
	$("#btnRegisterEmail").click(function(){
		var email=$("#txtEmailRegister").val();
		if(email.length){
			if(!validateEmail(email)){
				alert("Please input a validate email");
				$("#txtEmailRegister").focus();
				return false;
			}			
			$.ajax({
				  type: "POST",
				  url: '/ajax/utility/subscribe',
				  data: {email:email},
				  success: function(response){
					  //alert(response);
					  if( $.trim(response)=="RESULT_OK"){						  
						  alert("Email is subscribe! Thank you.");
					  }
				  },
			});
		}else{			
			alert("Please input email");
			$("#txtEmailRegister").focus();
		}
	});
});
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}